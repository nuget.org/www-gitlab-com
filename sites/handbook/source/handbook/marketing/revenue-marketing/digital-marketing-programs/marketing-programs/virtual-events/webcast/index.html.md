---
layout: markdown_page
title: "Webcast"
---

# [This page is being moved to the Marketing Virtual Event handbook page](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/). Please update your bookmarks.

# Overview
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking): [https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#overview](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#overview)



## Campaign Webcasts

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking): [https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#campaign-webcasts](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#campaign-webcasts)

### Submitting a campaign webcast idea

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking): [https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#campaign-webcast-idea](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#campaign-webcast-idea)

### Organizing campaign webcast epics and issues

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#campaigns-project-management](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#campaigns-project-management)

## Partner Webcasts

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#partner-webcasts](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#partner-webcasts)

### Checking BrightTALK webcast calendar for partner webcast dates

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#bt-partner-webcasts](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#bt-partner-webcasts)

### Organizing GitLab-Hosted Partner Webcast Epics and Issues

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#partner-project-management](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#partner-project-management)

### Organizing Partner-Hosted Partner Webcast Epics and Issues

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#partner-project-management](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#partner-project-management)

### Partner webcast tactical execution steps
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#partner-tactical-execution](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#partner-tactical-execution)

## Webcasts

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#field-abm-webcasts](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#field-abm-webcasts)

## Virtual Workshops

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#field-workshops](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#field-workshops)
# Best Practices

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#webcast-best-practices](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#webcast-best-practices)
## Speaker Approval

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#webcast-speaker-approval](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#webcast-speaker-approval)

## Tips for Speakers

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#speaker-tips](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#speaker-tips)

### Before Committing
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#speaker-before-committing](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#speaker-before-committing)

### Before the Dry Run
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#speaker-before-dry-run](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#speaker-before-dry-run)

### Before the Presentation
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#speaker-before-presentation](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#speaker-before-presentation)

# Logistical Set up
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#logistical-setup](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#logistical-setup) 

## Adding your webcasts into the calendar

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#add-to-calendar](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#add-to-calendar)

### Planned Webcasts

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#add-to-calendar](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#add-to-calendar) 

### Confirmed Webcasts

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#add-to-calendar](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#add-to-calendar) 

## LIVE webcast registration and tracking - Zoom

### Step 1: Configure Zoom

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#configure-zoom](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#configure-zoom) 

### Step 2: Set up the webcast in Marketo/SFDC, and integrate to Zoom
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zooom-mkto-integrate](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-mkto-integrate) 

#### Create program in Marketo - Zoom

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-program](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-program)  

#### Connect the Marketo program to Zoom via launchpoint integration
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-connect](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-connect) 

#### Create campaign in Salesforce - Zoom
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-sfdc-campaign](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-sfdc-campaign) 

### Step 3.A: Update Marketo Tokens

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-tokens](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-tokens) 

### Step 3.B: Turn on smart campaigns in Marketo
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-smart-campaigns](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-smart-campaigns) 

### Step 3.C: Create the landing page
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-lp](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-marketo-lp) 

##### Adjusting number of speakers in Marketo landing page

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-adjust-speakers](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-adjust-speakers) 

#### Multiple timeslot webcast - Zoom

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-multiple-timeslots](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-multiple-timeslots) 

#### Scheduling a multi-webcast series - Single landing page - Zoom
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-webcast-series](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-webcast-series)

### Webcast invitation - Zoom
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-webcast-invites](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-webcast-invites)


### Step 4: Add the webcast to the /events page
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-add-events-page](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-add-events-page)

### Step 5: Test your set up

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-testing](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-testing)


### Post LIVE webcast - Zoom

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-after-webcast](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-after-webcast)


#### Converting the webcast to an On-Demand gated asset - Zoom

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-on-demand-switch](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-on-demand-switch)

#### Test your follow up emails and set to send - Zoom

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-follow-up-emails](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-follow-up-emails)

#### Rescheduling a webcast - Zoom

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-reschedule](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-reschedule)

#### Canceling a webcast - Zoom

Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-cancel](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#zoom-cancel)

----
<!-- NOTE: THE REST OF THE DOCUMENTATION IS FOR BRIGHTTALK ONLY!!! -->

## LIVE webcast registration and tracking - BrightTALK
Please see the new handbook location below (and bookmark for future reference, as well as update any known linking):

[https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#](https://about.gitlab.com/handbook/marketing/virtual-events/webcasts/#)

### Create program in Marketo - BrightTALK

1. Create the webcast program in Marketo by navigating to the [Webcast program template for BrightTalk](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/ME6946A1)
1. Right-click and select "clone"
1. Next to "Clone To", choose `A campaign folder`.
1. For "Name", add the MKTO program name (this is also the SFDC campaign name). Use the following format: `YYYYMMDD_{Webcast Title}_[Region - only if applicable]`. For example, `20170418_MovingToGit`.
1. Under "Folder", choose the appropriate quarter within the  `GitLab-Hosted Campaign Webcasts` folder.
1. Click "Create" (note - you will create the SFDC campaign from Marketo in the next step!)

### Create campaign in Salesforce - BrightTALK

1. In the Marketo program Summary view, you will see `Salesforce Campaign Sync:` with a link that says "not set".
1. Click on "not set" 
1. Where it says "None", click the drop-down and choose "Create New"
1. The Marketo program name will auto-fill for the name (for consistency across both systems)
1. In the "Description", add a link to the epic
1. Click "Save"
1. NOW you will navigate to the Campaign in SFDC to do a quick review - [Shortcut to Campaigns](https://gitlab.my.salesforce.com/701/o)
1. Click into the SFDC campaign
1. Change the campaign owner to the webcast DRI
1. Change the status to `in progress`
1. Edit the `Enable Bizible Touchpoints` field to `Include only "Responded" Campaign Members`
1. Edit the Budgeted Cost (required) to cost of webcast, or "$1" if there is no cost associated
1. Click save

### Create webcast in BrightTALK

1. **LOGIN**: log into BrightTALK, go into the Content tab, and select `Add content`, schedule a talk. _*Note: Only type: `talk` allows you to share videos_
1. **TALK TITLE**: Insert the title of the webcast (public facing)
1. **DESCRIPTION**: Enter here a brief synopsis of the webcast. Keeping this description relevant to the content of the webcast will make it easier for viewers to search for it.
1. **PRESENTER**: Enter the name(s) of the presenter(s) who will be delivering the webcasts. Role and company can be included also.
1. **DURATION**: Add how long the webcast will be.
1. **START DATE**: Select the date your live webcast will take place on.
1. **START TIME**: Time your webcast will go live.
1. **TIMEZONE**: The time zone you select here should be based on where your presenter intends to present from. This will determine the local dial-in number generated for your presenter. NOTE: This will NOT affect how your webcast is listed. Webcasts are always listed in your player and on BrightTALK in the local time of your viewers.
1. **TAG**: Enter up to 10 terms that cover the topics and themes of your content - simply type each tag and click 'Add tag'. BrightTalk will suggest topics that are trending, but feel free to add any tag you believe is relevant - up to 34 characters per tag.
1. **IMAGE UPLOAD**: This will be used for the click to play overlay and thumbnail. Upload JPG/PNG image file of size 640x360. File upload limit 1MB.

PUBLISHING
1. **Public/Private**: Select 'Public' to promote this webcast in your channel listing and via the BrightTALK email service. Select 'Practice (Private)' to run this webcast without it being promoted in your channel listing or via the BrightTALK email service. Only viewers with a direct link to the 'Practice (Private)' webcast will be able to view it.
1. **Channel Survey**: Select 'Enabled' to allow surveys to go out for this webcast. Otherwise, select 'Disabled'.
1. **Campaign Reference**: Insert name of the Marketo Campaign **exactly** as shown in Marketo. Once added here, any changes to the Marketo program name will *BREAK* the sync! If you have a date change for your webcast, just leave it as-is in Marketo if the sync has already been set up.
1. **Add to BrightTalk Communities**: Control which BrightTALK communities to promote this webcast into by adding them below. The 'Primary community' you select will be the focus for promotional activities such as the BrightTALK email service.

#### Connect the Marketo program to BrightTALK
1. Navigate to the [Connectors Tab](https://www.brighttalk.com/central/account/20277/channel/17523/connectors) in BrightTALK Demand Central
1. Click to [Manage](https://www.brighttalk.com/central/account/20277/channel/17523/connector/1579/manage) under the Marketo Account
1. Navigate to `Marketo Programs`, find your program and select `Connect`
1. Insert program name EXACTLY as it is spelled in marketo, and click Next. *CRITIACL NOTE: If you change the program name, the sync will break.
1. Select how far back you want to sync data for, typically put in today's date. Click Next.
1. You are now connected! The sync runs every hour.

### Update tokens in Marketo program  - BrightTALK

1. There is no need to update ALL Tokens at this time, as all registration and emails are being sent from the BrightTALK platform. Update the following Tokens:
   * `{{my.bullet1}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet2}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet3}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet4}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.mpm owner email address}}` - not used in automation, but helpful to know who to go to about setup
   * `{{my.socialImage}}` - image that would be presented in social, slack, etc. preview when the URL is shared, this image is provided by design/social, leave the default unless presented with webcast specific image.
   * REPEAT this for speaker 2 and 3. If there are more or less speakers, follow the instructions below at the end of the general webcast setup.
   * `{{my.utm}}` - UTM to track traffic to the proper campaign in reporting dashboards (append integrated campaign utm or program name, if webcast is not part of an integrated campaign, to the utm campaign token)
   * `{{my.valueStatement}}` token with the short value statement on what the viewer gains from the webcast, this ties into the follow up emails and must meet the max/min requirements of the [character limit checker](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1471341556)
   * `{{my.webcastDate}}` - the webcast LIVE date.
   * `{{my.webcastDescription}}` - 2-3 sentences with approved character limits, this will show up in page previews on social and be used in Youtube and Pathfactory description.
   * `{{my.webcastSubtitle}}` token with subtitle for the webcast.
   * `{{my.webcastTime}}` token with the webcast time in local timezone/UTC timezone.
   * `{{my.webcastTitle}}` token with the webcast title.


### Activate smart campaigns in Marketo  - BrightTALK
   * Activate the `01 Processing` campaign.
   * Schedule the `02 Set No-Show Stats` smart campaign for 3-4 hours AFTER the webinar will end.
   * Interesting moments are captured on a global level.

### Webcast invitation  - BrightTALK

:exclamation: **Note from @jgragnola: we are working on further templatizing these invitations so that copy changes are not needed and tokens take care of these emails.** ([issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/3422))

1. Update emails `invitation 1 - 2 weeks prior`,  `invitation 2 - 1 week prior` , and if needed `invitation 3 - Day before` with relevant copies related to the webcast. *Note: We normally use the same copy for all 3 emails and simply tweaked the templated subject lines to sound more like “Reminders”.*
2. Approve copy and send samples to the requestor, and the presenter (if different from requestor).
3. Go to the List folder and edit the `Target List` smart list and input the names of past similar programs and applicable program statuses to the `Member of program` filter. This will make sure people that have attended programs with similar topics in the past are included in the invite.
4. Once you get approval on the sample email copy, schedule the email programs outlined in step 1.

### Add the webcast to the /events/ and /resources/ pages  - BrightTALK
*  To add the webcast to the /events page follow this [step by step guide](/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents).
*  To add the webcast to the /events page follow this [step by step guide](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#add-new-content-to-the-resources-page).

### Test your set up  - BrightTALK

1. Submit a test lead using your gitlab email on the LIVE landing page to make sure the registration is tracked appropriately in the Marketo program and you get a confirmation email from BrightTALK. It will take up to 2 hours to sync the regisration from BrightTalk to Marketo.

### Schedule your practice session  - BrightTALK
* Create a Talk, following the [instructions above.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#create-webcast-in-brighttalk)
* Select 'Practice (Private)' to run this webcast without it being promoted in your channel listing or via the BrightTALK email service. Only viewers with a direct link to the 'Practice (Private)' webcast will be able to view it.
* After the practice session, the system will automatically `Publish` your dry run. When your dry run completes, go into the Content section of your channel, Manage the dry run, click Edit, and change it to `Unpublish`.
* If you watch the replay of the practice session and notice that slides, video, or demo look grainy, select the gear button on the video and change the resolution of the webcast.

### Loading a video to replay
* You must upload the video you plan to play in a live talk prior to the event.
1. **LOGIN**: log into BrightTALK, go into the Content tab, and select `Add content`, Upload a Video. Videos are uploaded as Unpublished.
1. **WEBCAST TITLE**: Insert the title of the video (public facing)
1. **DESCRIPTION**: Enter here a brief synopsis of the video. Keeping this description relevant to the content of the webcast will make it easier for viewers to search for it if you plan to make this public.
1. **PRESENTER**: Enter the name(s) of the presenter(s) who will be delivering the webcasts. Role and company can be included also.
1. **DATE**: Select the date of upload (or date of the video)
1. **TAG**: Enter up to 10 terms that cover the topics and themes of your content - simply type each tag and click 'Add tag'. BrightTalk will suggest topics that are trending, but feel free to add any tag you believe is relevant - up to 34 characters per tag.
1. **DON'T PROMOTE**: If you only want to play this video in a live webcast, select `Don't promote this webcast into any communities`
1. Click Proceed
1. Select the video for upload. Add the time stamp to capture an image, or upload a featured image.
1. Select the nearest upload location to you.
1. Click upload. This may take awhile, depending on how large the video is.
1. After the video uploads, click `Edit and publish`. Change the video to `Private` if you are going to play it during a live webcast.
1. You can now select the video to play from the presenter screen of your Talk.

### Switching Live > On-Demand - BrightTALK
BrightTALk will automatically convert the video to on-demand in the BrightTALK platform. The steps below allow us to further leverage the webcast in Pathfactory.

1. **Youtube**: Upload the recording to our main GitLab channel
   * Fill in the title with the webcast title matching the Marketo token (`{{my.webcastTitle}}`)
   * Fill in the description with the short description matching the Marketo tokens (`{{my.contentDescription}}`)
   * Make sure the video is set as `Unlisted` so only people with the URL to the video can find it
1. **Youtube**: Once the recording has been uploaded, copy the video link on the right
1. **Pathfactory**: Login to PathFactory and add the copied youtube link to Pathfactory as new content by following the instructions outlined [here](/handbook/marketing/marketing-operations/pathfactory).


#### Rescheduling a webcast - BrightTALK

In the event you need to change the date of your webcast, please follow the steps outlined below.

1. DO NOT UPDATE THE PROGRAM NAME IN MARKETO - this will break the sync if it is already set up between Marketo and BrightTALK.
1. Update the date/time of the webcast on the webcast calendar and resend invites to all panelists.
1. Update the webcast epic and subsequent issues so the new date is reflected on the title and issue due dates are updated based on the new timeline.
1. Leave a comment on the epic stating the event has been rescheduled and tag all internal panelists and hosts.
1. Update on the [events page](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/events.yml) and [resources page](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/resources.yml).
1. Update the date in BrightTALK - an email will automatically send from BrightTALK to the registrants.


#### Canceling a webcast - BrightTALK

In the event you need to cancel your webcast, please follow the steps outlined below. Note that BrightTalk sets up promotional emails 24-48 hours in advance. This means that after you cancel a webcast, it may still appear in promotional emails (via webcast recommendations) from BrightTalk for up to 48 hours. However, they will not be able to register for the cancelled webcast.

1. Remove the webcast from the BrightTALK calendar.
1. Add [Cancelled] to the webcast epic and subsequent issues title then close it out.
1. Leave a comment on the epic stating the event has been canceled and tag all internal panelists and hosts.
1. If webcast is on the Events Page and Resources Page, remove in a new MR.
1. Go into BrightTalk and Cancel.
1. BrightTalk will automatically send an email to all registrants.
1. In the Marketo program, deactivate all active smart campaigns and append [Cancelled] to the program name. NOTE: once you do this, the sync between Marketo and BrightTALK will be broken and there is no reverese.
1. Go to Salesforce, append [Cancelled] to the SFDC campaign name.

## LIVE webcast registration and tracking - WebEx

### Step 1: Configure WebEx

*Note: The WebEx license can only be used for a single session at a time. This license is used for all field-marketing-run internally hosted webcasts. Therefore, when a webcast is requested please confirm there is not going to be a conflict between the pre-scheduled sessions - both live and dry-run - using that license by checking the [webcast gcal](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t). Schedule no less than 30min between sessions (before & after) so there is less chance of conflict and allows for a buffer.*
*IMPORTANT: You can only use the WebEx account that is not tied to the SSO to schedule webinars. The account to be used is `wbxmeet7@gitlab.com`, you can find the credentials in the 1Password vault `GitLab Webex Marketing Vault`*
*Note: Registration Confirmation and reminder emails will not be sent automatically from WebEx. Those will have to be sent by Marketo. The registration email is integrated in the `01 Registration Flow` smart campaign. Reminder emails will have to be scheduled and the tokens edited depending on the timeframe desired to send reminders.* 
1. **LOGIN**: log into WebEx,  go to the bottom left side and click on `WebEx Events(classic)`. In the left navbar, click on `Schedule an Event`. *It is imperative to use the WebEx classic interface otherwise the integration will not be sucessfull*
3. **Event Name**: add the topic as follows “Webcast title - Month DD, YYYY - HH:MM am/pm PT/HH:MM am/pm UTC” (for example: `Debunking Serverless security myths - October 21, 2019 - 8:30 am PT/3:30 pm UTC`).
4. **DESCRIPTION**: add a sentence to describe what the webcast is about at a high-level.
5. **WHEN**: add the webcast date and time.
6. **DURATION**: add how long the webcast will be PLUS 45 minutes. You must include an additional 45 minutes for the prep call before the event plus padding for running over, otherwise the Launchpoint integration will fail. Keep the start time as the actual time attendees should join, but increase the duration. For example, if your webcast is from 9:00am-10:00am PT, enter start time of 9:00am, but a duration of 1 hour and 45 minutes.
7. **TIMEZONE**: select the correct timezone for your webcast.
8. **DO NOT** change all the other settings that are prepopulated by the template.
9. **ALTERNATIVE HOSTS**: add webcast DRI, internal speaker(s), and Q&A resource as alternative hosts.
10. **PANELISTS**: add external GitLab speakers as panelists by following the video instructions below. 
11. **EMAILS**: uncheck email confirmation emails and reminder emails because we will send those from Marketo.



### Step 2: Set up the webcast in Marketo/SFDC, and integrate to WebEx

#### Create program in Marketo - WebEx

1. Create the webcast program in Marketo by navigating to either the [Webcast program template](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/ME8983A1).
1. Right-click the appropriate template, and select "clone"
1. Next to "Clone To", choose `A campaign folder`.
1. For "Name", add the MKTO program name (this is also the SFDC campaign name). Use the following format: `YYYYMMDD_{Webcast Title}_[Region - only if applicable]`. For example, `20170418_MovingToGit`.
1. Under "Folder", choose the appropriate quarter within the  `GitLab-Hosted Campaign Webcasts` or `GitLab-Hosted Workshops` folders.
1. Click "Create" (note - you will create the SFDC campaign from Marketo in the next step!)

#### Connect the Marketo program to WebEx via launchpoint integration
1. In the Marketo program Summary view, you will see `Event Partner:` with a link that says "not set".
1. Click on "not set"
1. In the Event Partner drop down, select `WebEx` and in the Login drop down, select `WebEx API`.
1. In the Event drop-down, select the name of the WebEx webcast you set up in [Step 1: Configure WebEx](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#step-1-configure-webex)

#### Create campaign in Salesforce - WebEx

1. In the Marketo program Summary view, you will see `Salesforce Campaign Sync:` with a link that says "not set".
1. Click on "not set" 
1. Where it says "None", click the drop-down and choose "Create New"
1. The Marketo program name will auto-fill for the name (for consistency across both systems)
1. In the "Description", add a link to the epic
1. Click "Save"
1. NOW you will navigate to the Campaign in SFDC to do a quick review - [Shortcut to Campaigns](https://gitlab.my.salesforce.com/701/o)
1. Click into the SFDC campaign
1. Change the campaign owner to the webcast DRI
1. Change the status to `in progress`
1. Edit the `Enable Bizible Touchpoints` field to `Include only "Responded" Campaign Members`
1. Edit the Budgeted Cost (required) to cost of webcast, or "1" if there is no cost associated
1. Click save

### Step 3.A: Update Marketo Tokens

Buckle up! There are a lot of tokens, but for good reason. This is an **advanced practice** and **best practice** within Marketo templates to increase efficiency and speed. Updating these at the top level of the program allows them to cascade through the landing page, emails, automation, and alerts creating a significantly more efficient process of launching new webcasts.
   * `{{my.bullet1}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet2}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet3}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet4}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.emailConfirmationButtonCopy}}`  - copy for the email confirmation (when on demand), leave as `Watch now`
   * `{{my.formButtonCopy}}` - copy for the form button, leave as `Register now` (when switching to on-demand, this will change to `Watch now`)
   * `{{my.formHeader}}` - copy for header of form, leave as `Save your spot today!` (when switching to on-demand, this will change to `View the webcast today!`)
   * `{{my.heroImage}}` - image to display above landing page form ([options in Marketo here](https://app-ab13.marketo.com/#FI0A1ZN9784))
   * `{{my.introParagraph}}` - intro paragraph to be used in landing page and nurture email, with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.mpm owner email address}}` - not used in automation, but helpful to know who to go to about setup
   * `{{my.ondemandUrl}}` - skip updating in initial registration page setup (update during on-demand switch), Pathfactory link WITHOUT the `https://` NOR the email tracking part (`lb_email=`)
     * Example of correct link to include: `learn.gitlab.com/gartner-voc-aro/gartner-voc-aro` - the code in the Marketo template assets will create the URL `https://learn.gitlab.com/gartner-voc-aro/gartner-voc-aro?lb_email={{lead.email address}}&{{my.utm}}`
     * Note that both parts of this url include custom URL slugs which should be incorporated into all pathfactory links for simplicity of tracking paramaeters
   * `{{my.socialImage}}` - image that would be presented in social, slack, etc. preview when the URL is shared, this image is provided by design/social, leave the default unless presented with webcast specific image.
   * `{{my.speaker1Company}}` token with speaker 1's company name
   * `{{my.speaker1ImageURL}}` token with speaker 1's image url in marketo design studio
   * `{{my.speaker1JobTitle}}` token with speaker 1's job title
   * `{{my.speaker1Name}}` token with speaker 1's full name
   * REPEAT this for speaker 2 and 3. If there are more or less speakers, follow the instructions below at the end of the general webcast setup.
   * `{{my.utm}}` - UTM to track traffic to the proper campaign in reporting dashboards (append integrated campaign utm or program name, if webcast is not part of an integrated campaign, to the utm campaign token)
   * `{{my.valueStatement}}` token with the short value statement on what the viewer gains from the webcast, this ties into the follow up emails and must meet the max/min requirements of the [character limit checker](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1471341556)
   * `{{my.webcastDate}}` - the webcast LIVE date.
   * `{{my.webcastDescription}}` - 2-3 sentences with approved character limits, this will show up in page previews on social and be used in Youtube and Pathfactory description.
   * `{{my.webcastSubtitle}}` token with subtitle for the webcast.
   * `{{my.webcastTime}}` token with the webcast time in local timezone/UTC timezone.
   * `{{my.webcastTitle}}` token with the webcast title.
   * `{{my.registrationConfirmationButtonCopy}}` token with the registration confirmation button message.
   * `{{my.webcastReminder1}}`: token with the time reminder value for the first reminder
   * `{{my.webcastReminder2}}`: token with the time reminder value for the second reminder
   * `{{my.Add To Calendar}}`: token for Add to Calendar open for ICS file. Double click on it and edit the time slot and descruption

### Step 3.B: Turn on smart campaigns in Marketo
   * Activate the `00 Interesting Moments` campaign.
   * Activate the `01a Registration Flow (single timeslot)` smart campaign.


### Step 3.C: Create the landing page

* When you cloned the webcast template, and update the Marketo tokens, your landing page is almost ready to go!
   * Under "Assets" right-click on `Registration Page` and hover over `URL Tools` > `Edit URL Settings`
   * Use the format `webcast-topic` (or `webcast-topic-region` if region is relevant) - ex. `webcast-mastering-cicd` or `webcast-mastering-cicd-italian`
* Complete the same steps for the `Thank You Page`
   * Use the format `webcast-topic-thank-you` (or `webcast-topic-region-thank-you` if region is relevant) - ex. `webcast-mastering-cicd-thank-you` or `webcast-mastering-cicd-italian-thank-you`

##### Adjusting number of speakers in Marketo landing page

**Less Speakers**
The speaker module is controlled in the Marketo landing page module. The template is initially set up to support three speakers (note: this is supported in both the My Tokens and the landing page template). If there are less speakers, follow the instructions below:
1. Right click on the Registration Landing Page and click `Edit Draft`
2. Double click on the `Speaker` section
3. Click `HTML` on the toolbar
4. Remove the code below for each speaker you need to remove

```
<div><br /></div>
<ul>
<li>{{my.speaker3ImageURL}}</li>
<li>{{my.speaker3Name}}</li>
<li>{{my.speaker3JobTitle}}</li>
<li>{{my.speaker3Company}}</li>
</ul>
```

**Less Speakers**
The speaker module is controlled in the Marketo landing page module. The template is initially set up to support three speakers (note: this is supported in both the My Tokens and the landing page template). If there are less speakers, follow the instructions below:
1. Right click on the Registration Landing Page and click `Edit Draft`
2. Double click on the `Speaker` section
3. Click `HTML` on the toolbar
4. Remove the code below for each speaker you need to remove

```
<div><br /></div>
<ul>
<li>{{my.speaker3ImageURL}}</li>
<li>{{my.speaker3Name}}</li>
<li>{{my.speaker3JobTitle}}</li>
<li>{{my.speaker3Company}}</li>
</ul>
```

If additional assistance is required, please comment in the [#marketing_programs slack](https://gitlab.slack.com/archives/CCWUCP4MS) for assistance if needed.

### Webcast invitation - WebEx

:exclamation: **Note from @jgragnola: we are working on further templatizing these invitations so that copy changes are not needed and tokens take care of these emails.** ([issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/3422))

1. Update emails `invitation 1 - 2 weeks prior`,  `invitation 2 - 1 week prior` , and if needed `invitation 3 - Day before` with relevant copies related to the webcast. *Note: We normally use the same copy for all 3 emails and simply tweaked the templated subject lines to sound more like “Reminders”.*
2. Update email `Registration Confirmation`, and if desired `Reminder 1` , and  `Reminder 3` with relevant copies related to the webcast. *Note: We normally use the same copy for all 3 emails and simply tweaked the templated subject lines to sound more like “Reminders”.*
3. Approve copy and send samples to the requestor, and the presenter (if different from requestor).
4. Go to the List folder and edit the `Target List` smart list and input the names of past similar programs and applicable program statuses to the `Member of program` filter. This will make sure people that have attended programs with similar topics in the past are included in the invite.
5. Once you get approval on the sample email copy, schedule the email programs outlined in step 1.
   

### Step 4: Add the webcast to the /events page
*  To add the webcast to the /events page follow this [step by step guide](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents).

### Step 5: Test your set up

1. Submit a test lead using your gitlab email on the LIVE landing page to make sure the registration is tracked appropriately in the Marketo program and you get a confirmation email. * Check and test the registration confirmation email. Do not forget to update the `Add to calendar` token and the email values for the Google calendar in the Registration Confirmation, Reminder 1 and Reminder 2.*

