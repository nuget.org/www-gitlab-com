---
description: "How GitLab approaches the software compliance solution including messaging and key resources to help marketing and sales."
layout: markdown_page
title: "DevOps Solution: Software Compliance"
---


## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Who to contact

|     Product Marketing    |    Technical Marketing    |
| ------------------------ | ------------------------- |
| Cindy Blake ( @cblake )  | Fernando Diaz ( @fjdiaz ) |

# The Market Viewpoint

## Software compliance

The Software Compliance solution is applicable for customers who are concerned about securing their software supply chain and simplifying their compliance with common industry regulations while at the same time speeding their software velocity.

GitLab's platform approach seamlessly embeds security and compliance within the DevOps platform, providing simplicity, visibility, and control. 

### Why is compliance a new concern?

While compliance and auditability has always been important, these requirements have greater attention following high-profile attacks on software supply chains and the related US President's [Executive Order to improve cyber security](https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/). 

Application security testing is still a foundational part of compliance, but now visibility and control across the entire software factory is even more paramount. 

### Desired business outcomes

* Meet industry regulatory requirements - efficiently and without slowing velocity of development
* Secure the software supply chain with better end-to-end governance to reduce security and compliance risks


### Personas

#### User Personas

**[Cameron the Compliance Manger](/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager)** 

Cameron needs to be sure all the company's development processes are compliant. Given the amount of data that a software development and delivery lifecycle produces, and the complexity of typical DevOps tool chains, he finds it difficult to find, aggregate, and report on all of the necessary data and changes made across systems for audit purposes. He needs to easily see who changed what, where, and when from end-to-end across the SWDLC. He needs the information to be available quickly and easily so he can reduce the time and disruption involved in the evidence collection process.

**The Developer** uses GitLab primarily within the MR pipeline report

The developer cares about security but does not want to become a compliance expert. Capabilities that help them run fast while staying compliant are appreciated.

**[Sam the Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)** may be tasked with automating and reporting on compliance policies so would like them to be simple, efficient, and automated wherever possible.


#### Buyer Personas

**The Security Manager or CISO (Sam's boss) or head of risk is usually the buyer for the Ultimate tier**

The key to winning their hearts is to focus on **Simplicity and control**
* Complexity is one of the CISO's chief complaints. Using one tool to provide visibility and control across the entire SDLC is valuable. 
* CISOs likely feel out of control, or at least pressured to be secure amidst evolving threats, high-profile cyber attacks, new compliance concerns, and development tool sprawl. It's hard to manage these software risks.  GitLab's single platform that provides end-to-end simplicity, visibility, and control they need.

## Industry Analyst Coverage

Analysts have not identified a market segment for software compliance. They have been writing articles about it though. Forrester spoke about the Executive Order at GitLab's Commit event in Fall of 2021.

## Market Requirements (in priority order)

| Market Requirements | Description | Typical capability-enabling features | Value/ROI |
| ------ | ------ | ------ | ------ |
| Common compliance controls |  Controls necessary to protect the integrity of the software development and deployment process | Role-based access, MR approvals, and many others | Simplify audit and compliance and reduce risk of noncompliance. |
| Automated policy enforcement | Automation can reduce the audit burden. Enforcing policies within the MR shifts compliance left where developers can resolve problems early in the life cycle | locked CI templates that enforce policies in the pipeline | Avoids late rework. In regulated industries, there is an approved change order window. if it is missed for rework, the change management process must start over.|
| Audit reporting | Audit events should be automatically captured and reported. Changes to code, controls, and IaC should be traceable and captured as audit events across the entire SDLC. | Audit events, audit reporting | reduce risk of non-compliance and efficiently identify root causes following a security or compliance incident |
| Security Governance | The solution automatically applies security policies against code to ensure that only appropriate risks are taken. Application vulnerabilities, representing risk, are tracked, managed, and reported. The solution enables routine assessments of security practices to evaluate for risk, compliance, audit and process improvement opportunities (usually for education purposes). |  Security policy automation, Risk and compliance reporting, Audit reporting, Variety of security metrics and process reporting, Vulnerability database and management | Efficiently monitor, manage and mitigate risk. Ability to identify exceptions and refine policies over time. |
| Security guardrails (Preventative - Pre CI/CD) | Preventative Application Security uses guardrails to help teams consistently build things that are secure from the start. | Compliant pipelines that cannot be circumvented by a developer, pre-approved code libraries, and auto-discovery that catalogs all third party code. | Prevents creating new vulnerabilities. |


# The GitLab Solution

## How GitLab Meets the Market Requirements

GitLab Software Compliance solution overview
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/XnYstHObqlA" frameborder="0" allow="accelerometer; autoplay;  encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>


| Market Requirements | How GitLab Delivers | GitLab Category | Demos |
| ------ | ------ | ------ | ------ |
| Common compliance controls |  GitLab provides [many common controls](https://docs.gitlab.com/ee/administration/compliance.html) throughtout the SDLC | Access and Compliance within the Manage stage | [![Compliance pipelines](../../images/youtube_social_icon_red-32x23.png) Compliance pipelines](https://www.youtube.com/watch?v=jKA_e_jimoI) |
| Automated policy enforcement | 
| Audit reporting |
| Security Governance | Security Policy Automation, Compliance Assessment, Security Risk Assessment, Audit Assessment | [Security Dashboards](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), [Audit events](https://docs.gitlab.com/ee/administration/audit_events.html) [Compliance Management](https://about.gitlab.com/direction/manage/compliance-management/) | |
| Security guardrails (Preventative - Pre CI/CD) |  | [bill of materials feature](https://docs.gitlab.com/ee/user/application_security/dependency_list/) | [![Manage your Application Dependencies with GitLab](../../images/youtube_social_icon_red-32x23.png) Manage your Application Dependencies with GitLab](https://youtu.be/scNS4UuPvLI)|



## Top Differentiators

| Differentiator | Value | Proof Point  | Demos |
| ----------------- | ------------- | --------------- | ------ |
| **Block MR based on Security Policy** | Bring Development and Security Teams closer by allowing security teams to apply organizational security policies before hand and review/approve security exceptions before the code is merged | **-**  | [![Merge-Request Approvals as Displayed in DevSecOps Overview](../../images/youtube_social_icon_red-32x23.png) Merge-Request Approvals as Displayed in DevSecOps Overview](https://youtu.be/XnYstHObqlA?t=174) |
| **Compliance Management** | GitLab makes compliance easier by providing a single source of truth for Dev, Sec and Ops through a single data-store. Everything is audited and for every change, there is a single thread that contains the full audit log of every decision and action - making audit compliance a breeze | The auditor for [Glympse](/customers/glympse/) observed that the company had remediated security issues faster than any other company that he had worked with before in his 20-year career. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates | [![Manage Compliance with GitLab](../../images/youtube_social_icon_red-32x23.png) Manage Compliance with GitLab](https://youtu.be/QV2dIocn-hk) |
| **Compliant pipelines** | tbd | **-** | tbd |

### How the GitLab DevOps platform helps achieve DevSecOps

**Simplicity/efficiency**  
* A single platform avoids is easier to manage and enables greater end-to-end visibility and control.  

**Visibility**  
* A single platform improves allows the organization to see who changed what, where, and when across the entire SDLC.

**Control**  
* Security policies can be automatically applied to all pipelines for consistency, compliance, and simplified audits
* [Role-based access controls](https://docs.gitlab.com/ee/user/permissions.html) provide seperation of duties and protect against malicious insider threats and accidental events
* A breadth of additional [compliance controls](https://docs.gitlab.com/ee/administration/compliance.html) provide superior governance over software development, delivery, and use.

### What Are The GitLab Advantages?

**Platform approach**. With a single platform for the entire SDLC, governance is greatly simplified and it is more effective.

**Contextual**.

**Seamless**. 

**Efficient and automated**.

## Message House

The [message house](./message-house/) for compliance provides a structure to describe and discuss the value and differentiators for the use case. (to be added)

Key message: GitLab helps you take control of your software development with a single platform that helps you automate and standardize the development process and policies while providing end-to-end visibility/traceability so that development can run fast with less risk.

## Competitive Comparison

See how we compare against other DevOps approaches 
  
1. Role-based access control (RBAC) for separation of duties. Competitive products's roles are broader and when a person changes roles, his/her permissions must be changed manually. Why is this important? If someone has access to push to prod and is demoted or moves to another group, you'd want the permissions to change automatically to avoid insider threats.  
1. Our workflows include compliance within MR approvals. No manual checks that impact velocity. (In essence, we shift left compliance also.)
1. [External status checks](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html) is an important feature for regulated industries. Changes are approved and must be pushed to production within a given timeframe. Delays can cause the approval process to start over.
1. With GitLab we have projects and groups where projects inherit policies from the group. Competitors cannot structure policies as flexibly as GitLab, an important feature for enterprise users. Examples include group level runners. 
1. Compliant pipelines allow GitLab users to select their compliance framework (e.g. PCI, HIPPA, etc) and those policies are used - and the developer cannot disable it (due to RBAC)

## Key Value by tier

### Free and Premium

**Key Compliance features with Free/Premium:**
* [Security Approvals in Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/settings.html#security-approvals-in-merge-requests) (available in Premium)

In addition, some security scanning is available:
* [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/) - check for potential security issues by evaluating static code.
* [Secrets Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/) - avoid exposing secrets and credentials for potential exploit.
* Container security at runtime for container host and container network security.


### Ultimate

**Key Compliance features with Ultimate:**

* [Compliance Dashboard](https://docs.gitlab.com/ee/user/compliance/compliance_dashboard/index.html) - See if merge requests were approved, and by whom.
* Fuzz testing, called out by EO
* [License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html) - identify the presence of new software licenses included in your project and track project dependencies. Approve or deny the inclusion of a specific license.

In addition, more security scanners are available, along with Vulnerability management and security dashboard. See DevSecOps solution for details. 


| Feature / Scenario                                  |    Free   |    Premium  | Ultimate | Product Analytics | Notes                      |
| --------------------------------------------------- | :-------: | :-------: | :------: | :---------------: | :------------------------- |
| Adopt GitLab Flow                                   |     X     |      X    |    X     |                   |                            |
| Try / Utilize Auto DevOps                           | *Partial* | *Partial* |    X     |                   |                            |
| Automated Testing with CI                           |     X     |     X     |    X     |                   |    Only SAST at all tiers                        |
| Review app                                          |     X     |     X     |    X     |                   | Needed to run DAST in CI/CD pipeline |
| [Merge Request Approval Flow / Rules](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/index.html)                 |           |     X     |    X     |   counts.merged_merge_requests_using_approval_rules            |                            |
| Protected Environments                              |           |     X     |    X     |                   |                            |
| [Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) |     X     |     X     |    X     |  container_registry_enabled |                            |
| [Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/) |     X     |     X     |    X     | counts_monthly.packages |                            |
| [SAST (Static Application Security Testing)](https://docs.gitlab.com/ee/user/application_security/sast/)                              |     X     |     X     |    X     | user_sast_jobs                | |
| [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)                                            |     X     |     X     |    X     | user_secret_detection_jobs    | |
| [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)                                        |           |           |    X     | user_container_scanning_jobs  | |
| [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)                                      |           |           |    X     | user_dependency_scanning_jobs | |
| [License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/)                                                  |           |           |    X     | user_license_management_jobs  | |
| [API Fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/)                                                      |           |           |    X     | user_api_fuzzing_jobs, user_api_fuzzing_dnd_jobs on self-managed | |
| [Coverage Fuzzing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)                                            |          |        |    X     | user_coverage_fuzzing_jobs    | |
| [Security Approvals](https://docs.gitlab.com/ee/user/application_security/#security-approvals-in-merge-requests)                      |           |           |    X     |                   |                            |
| Compliance Dashboard |           |           |    X     |                   |                            |

The table includes free/community and paid tiers associated with GitLab's self-managed and cloud offering.

- F/C = Free
- S/P = Premium
- G/U = Ultimate

## Technology Partnerships

We partner with key industry vendors to extend GitLab's ability to address customer needs and fulfil the market requirements.

Hashicorp Vault

A more complete list of technology partners can be found on our [security partners page](https://about.gitlab.com/partners/technology-partners/#security). If you or your customer has a third party they'd like to see integrated into GitLab, send them to the [partner integration page](https://about.gitlab.com/partners/technology-partners/integrate/) for instructions.


# Selling the Software Compliance Solution

## Customer Facing Slides
* Securing your software supply chain

## Discovery Questions

**Initial probe for direction. Where’s the pain?**

Integrating application security testing into Agile DevOps software development is difficult with many potential challenges. Use these 6 questions to probe a bit to see which areas are of most concern, then go deeper on those topics further below.

**1. Policy Automation**
* Are Security policies automated with security requirements built into the development process? Or are most projects secured a bit differently every time?
* Does automation enforce security standards? Are Security policies automated with security requirements built into the development process? Or are most projects secured a bit differently every time? How much manual intervention is needed?
* How difficult is it to evaluate compliance with policies and drift/exceptions?
* Compliance should be regularly evaluated and exceptions reviewed. Automating overly rigorous policies may prove detrimental to business objectives and may not be realistically achieved, so it is important to find a balance between compliance and efficiency.
* Do you have Visibility into access control, reporting, and change logs?
* Do you know how often policy exceptions are granted?
* Audit - How much time is spent doing audits? Can you see who made changes not only to the application code, but also to CI pipelines, configurations, templates, etc?

**2. Managing Risk**

* Are you mostly concerned about the OWASP Top 10. What if you could find the OWASP Top 10 for every code change and free your development workflow by decluttering their tasks to focus on vulns they just created and not technical debt from past efforts to avoid creating new technical debt? Would that help you avoid new technical debt and reduce risk?
* Do you focus on remediation of critical and high vulnerabilities? How much of your remediation is for medium and low vulns? Did you know that most exploits are against medium risk vulnerabilities? What if you could help developers find and fix each vuln as they are created?
* What percentage of code are you currently scanning? Are there holes where an attacker could more easily enter and then traverse laterally? How much more would it cost you to scan all of your code?
* If you are using containers, orchestrators, and/or microservices/API’s, how are you scanning them for vulnerabilities and monitoring them during production?
* What percentage of vulns found that require remediation are actually remediated? How quickly? (Mean time to remediation)
* Can you see how developers handled vulnerabilities that were found? Would it be valuable to have visibility into vulnerabilities and their risk earlier in the lifecycle? Would this help you with security audits?
* How much of the security team’s time is spent tracking vulnerabilities, triaging them, and following up to see that they were remediated?



## Potential Objections



## Proof Points - customers

### Quotes and reviews


### Customer Case Studies

**[Glympse](/customers/glympse/)**
* **Problem** A complex developer tech stack with over 20 distinct tools that was hard to maintain and manage. Teams spent several hours a week keeping tools running rather than shipping innovation to their app.
* **Solution:** GitLab Ultimate (SCM, CI, DevSecOps)
* **Results: ~20 tools consolidated into GitLab and remediated security issues faster than any other company in their Security Auditor's experience.
* **Sales Segment:** SMB

**[Chorus](https://about.gitlab.com/customers/chorus/)**
* **Problem:** The founders of Chorus built the tool from the beginning using GitLab.
* **Solution:** GitLab Ultimate (SCM, CI, DevSecOps)
* **Results:** **6 week production cycles reduced to 1 week** During a recent audit for SOC2 compliance, the auditors said that Chorus had the fastest auditing process they have seen and most of that is due to the capabilities of GitLab.
* **Sales Segment:** SMB

### References to help you close

[SFDC report of referenceable secure customers](https://gitlab.my.salesforce.com/a6l4M000000kDw2) Note: Sales team members should have access to this report. If you do not have access, reach out to the [customer reference team](/handbook/marketing/strategic-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact) for assistance.



### Additional Documentation Links

- [GitLab Security Compliance Controls](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
- [GitLab Security Practices](/handbook/security/)
- [Security Planning](/handbook/engineering/security/planning/)

### Enablement and Training

The following will link to enablement and training videos and content.

- [Handling Security Audits](https://www.youtube.com/watch?v=ziIJIec4w0g)
- [Adding Security to your pipelines](https://www.youtube.com/watch?v=Fd5DhebtScg&t=3s)

### Professional Service Offers

GitLab offers a [variety of pre-packaged and custom services](https://about.gitlab.com/services/) for our customers and partners. The following are service offers specific to this solution. For additional services, see the [full service catalog](https://about.gitlab.com/services/catalog/).

- [DevOps Fundamentals Training](https://about.gitlab.com/services/education/devops-fundamentals/) (all stages of the DevOps lifecycle)
- [GitLab CI/CD Training](https://about.gitlab.com/services/education/gitlab-ci/)
- [Integration Services](https://about.gitlab.com/services/integration/	)


## Resources

### Software security guide
* Coming soon

### Blogs
* Saumya's, Matts, more Coming soon

### Clickthrough & Live Demos
* [All Marketing Click Through Demos](/handbook/marketing/strategic-marketing/demo/#click-throughs)
* [All Marketing Live Demos](/handbook/marketing/strategic-marketing/demo/#live-instructions)

### Roadmap
* Manage Direction 

## Technical Resources for Solution Architects

## Buyer's Journey
[Inventory of key assets](https://docs.google.com/spreadsheets/d/15-yai90Ol7k4D2exHXqHXtFastR6FcE6HABD_GisAl8/edit#gid=0) in the buyer's Journey


